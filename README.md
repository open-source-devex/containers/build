[![pipeline status](https://gitlab.com/open-source-devex/containers/build/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/containers/build/commits/master)

# Docker container to run builds

Based on `alpine:latest` with tooling for building projects during CI.

## Container environment

This container sets a few docker environment variables by default. The default values are set to match gitlab shared runners.
For any other configuration set the environment variables at runtime:
* DOCKER_HOST
* DOCKER_DRIVER
* DOCKER_TLS_CERTDIR

## Building new containers without code changes

Because every commit to master in this repository creates a release that tagged in git, the best way to trigger a new container build without the code having changed is to create an empty commit.
Such builds are needed for example when the base container has been updated.

```bash
git commit --allow-empty -m "Trigger build of new container on latest base image"
```
