#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f "${CONTAINER_NAME}" 2>&1 > /dev/null || true

docker run --name "${CONTAINER_NAME}" \
 -e DOCKER_HOST="" \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -dt "${CONTAINER_TEST_IMAGE}"

timeout 15 docker logs -f "${CONTAINER_NAME}" || true

# tests
docker exec -t "${CONTAINER_NAME}" ash -c 'type vim'
docker exec -t "${CONTAINER_NAME}" ash -c 'type curl'
docker exec -t "${CONTAINER_NAME}" ash -c 'type wget'
docker exec -t "${CONTAINER_NAME}" ash -c 'type bash'
docker exec -t "${CONTAINER_NAME}" ash -c 'type git'
docker exec -t "${CONTAINER_NAME}" ash -c 'type jq'
docker exec -t "${CONTAINER_NAME}" ash -c 'type ssh'
docker exec -t "${CONTAINER_NAME}" ash -c 'type zip'
docker exec -t "${CONTAINER_NAME}" ash -c 'type unzip'
docker exec -t "${CONTAINER_NAME}" ash -c 'date +"%Y-%m-%dT%H:%M:%SZ" --utc -d "+1 hour"'
docker exec -t "${CONTAINER_NAME}" ash -c 'test -d /opt/toolbox' # directory exists
docker exec -t "${CONTAINER_NAME}" ash -c '/opt/toolbox/users/setup-bot.sh'
docker exec -t "${CONTAINER_NAME}" ash -c 'test -f ~/.ssh/config'
docker exec -t "${CONTAINER_NAME}" ash -c 'test -f ~/.gitconfig'
docker exec -t "${CONTAINER_NAME}" ash -c 'aws --version'
docker exec -t "${CONTAINER_NAME}" ash -c 'ecs-cli -v'
docker exec -t "${CONTAINER_NAME}" ash -c 'type bump.sh'
docker exec -t "${CONTAINER_NAME}" ash -c 'type kubectl'
docker exec -t "${CONTAINER_NAME}" ash -c 'type helm'
docker exec -t "${CONTAINER_NAME}" ash -c 'type envsubst'
docker exec -t "${CONTAINER_NAME}" docker info
docker exec -t "${CONTAINER_NAME}" docker --version | grep "20.10.16"
docker exec -t "${CONTAINER_NAME}" docker compose version


docker exec -t "${CONTAINER_NAME}" docker run --rm hello-world


# clean up
docker rm -f "${CONTAINER_NAME}"
